# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 22:29:36 2021

@author: embedded05
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import imp

lumapi = imp.load_source("Lumapi","C:\\Program Files\\Lumerical\\v211\\api\\python\\Lumapi.py")

ic1 = lumapi.INTERCONNECT("testH_LO.icp")

p1 = ic1.getresult("ONA_1","input 1/mode 1/gain")  #Ch1 - Qp
p2 = ic1.getresult("ONA_1","input 2/mode 1/gain")  #Ch2 - Qn
p3 = ic1.getresult("ONA_1","input 3/mode 1/gain")  #Ch3 - In
p4 = ic1.getresult("ONA_1","input 4/mode 1/gain")  #Ch4 - Ip
gc = ic1.getresult("ONA_2","input 1/mode 1/gain")

lbd = p1['wavelength']
gain1 = p1['TE gain (dB)']
gain2 = p2['TE gain (dB)']
gain3 = p3['TE gain (dB)']
gain4 = p4['TE gain (dB)']
gain_gc = gc['TE gain (dB)']

g1 = gain1 - gain_gc
g2 = gain2 - gain_gc
g3 = gain3 - gain_gc
g4 = gain4 - gain_gc

#Imbalance

Inb_Q = g1 - g2  #Inbalance Q = 10log(P_Qp/P_Qn)
Inb_I = g4 - g3  #Inbalance I = 10log(P_Ip/P_In)

fig,axs = plt.subplots(2,1,figsize =(10,6))
fig.text(0.07, 0.5, 'Imbalance (dB)',fontsize = 14,fontstyle = 'italic', ha='center', va='center', rotation='vertical')
fig.text(0.5, 0.04, 'lambda (nm)',fontsize = 14,fontstyle = 'italic', ha='center', va='center')

axs[0].plot(1e9*lbd,Inb_I,linewidth = 1.4,color = 'green',label = 'Canal I')
axs[0].set_xlim(1530,1565)
axs[0].grid()
axs[0].legend()

axs[1].plot(1e9*lbd,Inb_Q,linewidth = 1.4,color = 'blue',label = 'Canal Q')
axs[1].set_xlim(1530,1565)
axs[1].grid()
axs[1].legend()

# CMRR

CMRR_I = - 20*np.log10( np.abs( ( 10**(g4/10) - 10**(g3/10) ) / ( 10**(g4/10) + 10**(g3/10) ) ) )
CMRR_Q = - 20*np.log10( np.abs( ( 10**(g1/10) - 10**(g2/10) ) / ( 10**(g1/10) + 10**(g2/10) ) ) )

fig,axs = plt.subplots(2,1,figsize =(10,6))
fig.text(0.07, 0.5, 'CMRR (dB)',fontsize = 14,fontstyle = 'italic', ha='center', va='center', rotation='vertical')
fig.text(0.5, 0.04, 'lambda (nm)',fontsize = 14,fontstyle = 'italic', ha='center', va='center')

axs[0].plot(1e9*lbd,CMRR_I,linewidth = 1.4,color = 'green',label = 'Canal I')
axs[0].set_xlim(1530,1565)
axs[0].grid()
axs[0].legend()

axs[1].plot(1e9*lbd,CMRR_Q,linewidth = 1.4,color = 'blue',label = 'Canal Q')
axs[1].set_xlim(1530,1565)
axs[1].grid()
axs[1].legend()


#Insertion Loss

ic2 = lumapi.INTERCONNECT("testH_SIG.icp")   #Dados com o laser na entrada do sinal

P1 = ic2.getresult("ONA_1","input 1/mode 1/gain")  #Ch1 - Qp
P2 = ic2.getresult("ONA_1","input 2/mode 1/gain")  #Ch2 - Qn
P3 = ic2.getresult("ONA_1","input 3/mode 1/gain")  #Ch3 - In
P4 = ic2.getresult("ONA_1","input 4/mode 1/gain")  #Ch4 - Ip

lbd = P1['wavelength']
Gain1 = P1['TE gain (dB)']
Gain2 = P2['TE gain (dB)']
Gain3 = P3['TE gain (dB)']
Gain4 = P4['TE gain (dB)']

G1 = Gain1 - gain_gc
G2 = Gain2 - gain_gc
G3 = Gain3 - gain_gc
G4 = Gain4 - gain_gc

insLoss_LO = -10*np.log(10**(g1/10) + 10**(g2/10) + 10**(g3/10) + 10**(g4/10))
insLoss_SIG = -10*np.log(10**(G1/10) + 10**(G2/10) + 10**(G3/10) + 10**(G4/10))

fig,axs = plt.subplots(2,1,figsize =(10,6))
fig.text(0.07, 0.5, 'Total insertion loss (dB)',fontsize = 14,fontstyle = 'italic', ha='center', va='center', rotation='vertical')
fig.text(0.5, 0.04, 'lambda (nm)',fontsize = 14,fontstyle = 'italic', ha='center', va='center')

axs[0].plot(1e9*lbd,insLoss_LO,linewidth = 1.4,color = 'green',label = 'Input from LO port')
axs[0].set_xlim(1530,1565)
axs[0].grid()
axs[0].legend()

axs[1].plot(1e9*lbd,insLoss_SIG,linewidth = 1.4,color = 'blue',label = 'Input from signal port')
axs[1].set_xlim(1530,1565)
axs[1].grid()
axs[1].legend()

#Phase error
ic3 = lumapi.INTERCONNECT("test_angle.icp")   #Dados com o laser na entrada do sinal

a1 = ic3.getresult("ONA_1","input 1/mode 1/angle")
a2 = ic3.getresult("ONA_1","input 2/mode 1/angle") 
a3 = ic3.getresult("ONA_1","input 3/mode 1/angle") 
a4 = ic3.getresult("ONA_1","input 4/mode 1/angle")  
a5 = ic3.getresult("ONA_1","input 5/mode 1/angle")

phi1 = a1['TE angle (deg)']
phi2 = a2['TE angle (deg)']
phi3 = a3['TE angle (deg)']
phi4 = a4['TE angle (deg)']
teta = a5["'TE' angle (deg)"]


angle = (phi2 - phi1) + (phi4 - phi3) + teta
phase_error = 90 - (angle - 360*(angle//360))

plt.figure(4)
plt.plot(1e9*lbd,phase_error,linewidth = 1.4,color = 'blue')
plt.xlabel('lambda (nm)',fontsize = 14,fontstyle = 'italic')
plt.ylabel('Phase error (° )',fontsize = 14,fontstyle = 'italic')
plt.xlim(1530,1565)
plt.grid()